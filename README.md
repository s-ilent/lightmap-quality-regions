# Lightmap Quality Regions

An addon for Unity that lets you define bounding boxes that set the relative lightmapping quality of objects within them to a specified value.

## Installation 
Download the repository. Then place the LightmapQualityRegions/ folder with the scripts into your Assets/ directory.

## Usage

1. Create new GameObject in your scene. Add Lightmap Quality Regions and Box Collider components. Set the Tag to EditorOnly.
2. Set the Box Colliders to encompass the areas you want to set the quality of. Make sure that they are set as Triggers.
3. Set the settings on the Lightmap Quality Region.

   **Lightmap Scale** is what will be applied to individual objects. <br>
   Note that areas without quality regions will be set to a scale of 0, so you'll want to cover your scene with boxes where you want lightmapping. (Or make a really big box.)
   
   **Use Scale Ratio** will change the final scale set to objects based on their size.
   
   **Require Containment** will only apply the scale setting to objects within the box, rather than objects intersecting it. <br>
   For example, if there is a large mesh for a building and many smaller objects on it, both the building and the objects will be affected by a region touching them. However, with Require Containment, you can cover the top of the building and only affect the objects on it.
4. In the Tools menu, select **Apply Lightmap Quality Regions** to apply the settings.

Note that you can also place Lightmap Quality Regions on individual GameObjects to override their settings. 

# License
This work is licensed under the MIT license.