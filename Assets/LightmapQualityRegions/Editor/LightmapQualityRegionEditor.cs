using UnityEngine;
using UnityEditor;

namespace SilentTools.LightmapQualityRegion
{
    [CustomEditor(typeof(LightmapQualityRegionSettings))]
    public class LightmapQualityRegionEditor : Editor
    {
    	SerializedProperty lightmapScale;
    	SerializedProperty useScaleRatio;
    	SerializedProperty requireContainment;

        void OnEnable()
        {
            lightmapScale = serializedObject.FindProperty("lightmapScale");
            useScaleRatio = serializedObject.FindProperty("useScaleRatio");
            requireContainment = serializedObject.FindProperty("requireContainment");
        }

        public override void OnInspectorGUI()
        {
        	EditorGUILayout.LabelField("Apply from the Tools menu.");
            var region = (LightmapQualityRegionSettings)target;
            EditorGUILayout.DelayedFloatField(lightmapScale);
        	EditorGUILayout.LabelField("If attached to a box collider, meshes inside the collider will be affected.");
        	EditorGUILayout.LabelField("If attached to a renderer, will override any other region settings.");
            EditorGUILayout.PropertyField(useScaleRatio);
        	EditorGUILayout.LabelField("Scales the size in the lightmap by the size of the mesh bounds.");
            EditorGUILayout.PropertyField(requireContainment);
        	EditorGUILayout.LabelField("Only applies scale to meshes fully inside the volume, rather than meshes intersecting it.");

            serializedObject.ApplyModifiedProperties(); 
        }
    }
}