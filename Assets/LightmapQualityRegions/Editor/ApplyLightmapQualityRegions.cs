using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SilentTools.LightmapQualityRegion
{
    public class ApplyLightmapQualityRegions : MonoBehaviour 
    {
        [MenuItem ("Tools/Apply Lightmap Quality Regions")]
        static void generate () 
        {
            generateLightmapScale (1f);
        }

        // ref: https://answers.unity.com/questions/292467/check-if-is-encapsulated.html
        static bool BoundsIsEncapsulated(Bounds Encapsulator, Bounds Encapsulating)
        {
            return Encapsulator.Contains(Encapsulating.min) && Encapsulator.Contains(Encapsulating.max);
        }
        static bool ContainBounds(Bounds bounds, Bounds target)
        {
            return bounds.Contains(target.min) && bounds.Contains(target.max);
        }

        static void generateLightmapScale (float ratio) 
        {
            GameObject[] objectsInScene = UnityEngine.Object.FindObjectsOfType<GameObject> ();
            LightmapQualityRegionSettings[] regionsInScene = 
                UnityEngine.Object.FindObjectsOfType<LightmapQualityRegionSettings> ();

            // Todo: Handle objects where there is a renderer and a region (overriding other settings)
            // but which can affect other objects within the bounds of their collider.

            // The current handling is made for forcing specific objects to have a LOWER quality,
            // not a higher one.
                
            float defaultScale = 0;

            int stats = 0;
            foreach (GameObject obj in objectsInScene) 
            {
                if (obj.isStatic) 
                {
                    if (obj.GetComponent<Renderer> () != null) {
                        float scale = defaultScale;

                        LightmapQualityRegionSettings localRegion = obj.GetComponent<LightmapQualityRegionSettings>();
                        if (localRegion != null) {
                            Bounds objectVolume = obj.GetComponent<Renderer>().bounds;
                            scale = (localRegion.useScaleRatio
                            ? Mathf.Clamp (objectVolume.size.magnitude, 2.5f, 10f) / ratio
                            : ratio) * localRegion.lightmapScale;
                        } else {
                            foreach (LightmapQualityRegionSettings region in regionsInScene) 
                            {
                                Bounds regionVolume = region.GetComponent<Collider>().bounds;
                                Bounds objectVolume = obj.GetComponent<Renderer>().bounds;
                                if(region.requireContainment
                                    ? BoundsIsEncapsulated(regionVolume, objectVolume) 
                                    : regionVolume.Intersects(objectVolume))
                                {
                                    float setScale = region.useScaleRatio
                                    ? Mathf.Clamp (objectVolume.size.magnitude, 2.5f, 10f) / ratio
                                    : ratio;
                                    scale = Mathf.Max(scale, region.lightmapScale * setScale);
                                }

                            }
                        }

                        SerializedObject so = new SerializedObject (obj.GetComponent<Renderer> ());

                        so.FindProperty ("m_ScaleInLightmap").floatValue = scale;

                        stats++;

                        so.ApplyModifiedProperties ();
                    }

                }
            }
            Debug.Log("Set lightmap scale to " + stats + " meshes.");
        }
    }
}
