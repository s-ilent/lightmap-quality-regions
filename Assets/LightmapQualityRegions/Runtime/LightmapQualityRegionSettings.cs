using UnityEngine;

namespace SilentTools.LightmapQualityRegion
{
    [ExecuteInEditMode]
    //[RequireComponent(typeof(BoxCollider))]
    public class LightmapQualityRegionSettings : MonoBehaviour
    {
        public float lightmapScale = 1.0f;
        public bool useScaleRatio = false;
        public bool requireContainment = false;

        private void OnEnable()
        {
            // Removed to allow for placing on individual objects.
            /*
            var collider = GetComponent<BoxCollider>();
            collider.isTrigger = true;
            */
        }
    }
}